import express from 'express';
import cors from 'cors';
import mongoose from 'mongoose';
import { Model } from './models/user.models.js';
import jwt from 'jsonwebtoken';
import bcryptjs from 'bcryptjs';
import dotenv from 'dotenv';
import path from 'path';

import * as url from 'url';
dotenv.config({ path: './.env' });
const __filename = url.fileURLToPath(import.meta.url);
const __dirname = url.fileURLToPath(new URL('.', import.meta.url));
console.log(__dirname);
const port = process.env.PORT || 5000;
const secret = process.env.SECRET_KEY || 'some secret passphrase here for local development';

const app = express();

mongoose
	.connect(process.env.MONGODB_URI, { useNewUrlParser: true })
	.then((result) => {})
	.catch((error) => console.log(error));

//middleware
app.use(cors());
app.use(express.json()); // <--- this let express that we use json for data exchange. This will PARSE anything that comes into Req.body as JSON

app.post('/api/register', async (req, res) => {
	const newPassword = await bcryptjs.hash(req.body.password, 10);
	try {
		await Model.create({
			name: req.body.name,
			email: req.body.email,
			password: newPassword
		});
		res.json({ status: 'ok' });
	} catch (err) {
		res.json({ status: 'err', error: 'Duplicate email' });
	}
});

app.post('/api/login', async (req, res) => {
	const user = await Model.findOne({ email: req.body.email });

	if (!user) {
		return res.json({ status: 'error', error: 'Invalid user' });
	}
	const isPassword = await bcryptjs.compare(req.body.password, user.password);

	if (isPassword) {
		const token = jwt.sign(
			{
				name: user.name,
				email: user.email
			},
			secret
		);
		return res.json({ status: 'ok', user: token });
	} else {
		return res.json({ status: 'error', user: false });
	}
});

app.get('/api/quote', async (req, res) => {
	const token = req.headers['x-access-token'];

	try {
		const decoded = jwt.verify(token, secret);
		const email = decoded.email;
		const user = await Model.findOne({ email: email });

		return res.json({ status: 'ok', quote: user.quote });
	} catch (err) {
		return res.json({ status: 'error', error: 'Invalid token' });
	}
});

app.post('/api/quote', async (req, res) => {
	const token = req.headers['x-access-token'];
	console.log(token);

	try {
		const decoded = jwt.verify(token, 'secret123');
		const email = decoded.email;
		const user = await Model.updateOne({ email: email }, { $set: { quote: req.body.quote } });

		return res.json({ status: 'ok' });
	} catch (err) {
		return res.json({ status: 'error', error: 'Invalid token' });
	}
});

// Priority serve any static files.
app.use(express.static(path.resolve(__dirname, 'client', 'build')));

// All remaining requests return the React app, so it can handle routing.
app.get('*', function(request, response) {
	response.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
});

app.listen(port, () => {
	console.log('server started on 5000');
});

import { AppBar, Button, FormGroup, Grid, TextField, Toolbar, Typography } from '@mui/material';
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';

function Register() {
	const [ name, setname ] = useState('');
	const [ email, setEmail ] = useState('');
	const [ password, setPassword ] = useState('');
	const [ ConPassWord, setConPassWord ] = useState('');
	const navigate = useNavigate();

	const handleName = (e: React.ChangeEvent<HTMLInputElement>) => {
		setname(e.target.value);
	};

	const handleEmail = (e: React.ChangeEvent<HTMLInputElement>) => {
		setEmail(e.target.value);
	};

	const handlePassword = (e: React.ChangeEvent<HTMLInputElement>) => {
		setPassword(e.target.value);
	};

	const handleConPasswrod = (e: React.ChangeEvent<HTMLInputElement>) => {
		setConPassWord(e.target.value);
	};

	const handleSignUp = async (e: React.MouseEvent<HTMLButtonElement>) => {
		e.preventDefault();
		const response = await fetch(`${process.env.REACT_APP_MERN_URL}api/register`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				name,
				email,
				password
			})
		});
		const data = await response.json();
		if (data.status === 'ok') {
			navigate('/login');
		}
	};
	return (
		<React.Fragment>
			<AppBar position="sticky">
				<Toolbar>
					<Typography variant="h5" align="center" sx={{ width: '100%' }}>
						Register
					</Typography>
				</Toolbar>
			</AppBar>
			<Grid container item md={12} justifyContent="center" alignItems="center" sx={{ minHeight: '690px' }}>
				<FormGroup sx={{ width: '430px', marginY: '30px' }}>
					<TextField required variant="outlined" label="Enter Your Name" value={name} onChange={handleName} />
					<br />
					<TextField
						required
						error={email.length > 0 ? !email.includes('@') : false}
						variant="outlined"
						label="Enter Your Email"
						type="email"
						value={email}
						onChange={handleEmail}
					/>
					<br />
					<TextField
						required
						variant="outlined"
						label="Enter password"
						type="password"
						value={password}
						onChange={handlePassword}
					/>
					<br />
					<TextField
						required
						error={ConPassWord.length > 0 ? !(password === ConPassWord) : false}
						variant="outlined"
						label="Confirm password"
						type="password"
						value={ConPassWord}
						onChange={handleConPasswrod}
						helperText={!(password === ConPassWord) ? "Password Doesn't Match" : null}
					/>
					<br />
					<Button variant="contained" onClick={handleSignUp}>
						Submit
					</Button>
				</FormGroup>
			</Grid>
		</React.Fragment>
	);
}

export default Register;

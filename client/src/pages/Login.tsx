import { AppBar, Button, FormGroup, Grid, TextField, Toolbar, Typography } from '@mui/material';
import React, { useState } from 'react';

export const Login = () => {
	const [ email, setEmail ] = useState('');
	const [ password, setPassword ] = useState('');

	const handleEmail = (e: React.ChangeEvent<HTMLInputElement>) => {
		setEmail(e.target.value);
	};

	const handlePassword = (e: React.ChangeEvent<HTMLInputElement>) => {
		setPassword(e.target.value);
	};

	const handleLogin = async (e: React.MouseEvent<HTMLButtonElement>) => {
		e.preventDefault();
		const response = await fetch(`${process.env.REACT_APP_MERN_URL}api/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email,
				password
			})
		});
		const data = await response.json();
		if (data.user) {
			localStorage.setItem('token', JSON.stringify(data.user));
			alert('Login Successful');
			window.location.href = '/dashboard';
		} else {
			alert('Check your username and password');
		}
	};
	return (
		<React.Fragment>
			<AppBar position="sticky">
				<Toolbar>
					<Typography variant="h5" align="center" sx={{ width: '100%' }}>
						Login
					</Typography>
				</Toolbar>
			</AppBar>
			<Grid container item md={12} justifyContent="center" alignItems="center" sx={{ minHeight: '690px' }}>
				<FormGroup sx={{ width: '430px', marginY: '30px' }}>
					<br />
					<TextField
						required
						error={email.length > 0 ? !email.includes('@') : false}
						variant="outlined"
						label="Enter Your Email"
						type="email"
						value={email}
						onChange={handleEmail}
					/>
					<br />
					<TextField
						required
						variant="outlined"
						label="Enter password"
						type="password"
						value={password}
						onChange={handlePassword}
					/>
					<br />

					<br />
					<Button variant="contained" onClick={handleLogin}>
						Submit
					</Button>
				</FormGroup>
			</Grid>
		</React.Fragment>
	);
};

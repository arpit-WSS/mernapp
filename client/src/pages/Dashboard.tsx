import React, { useEffect, useState } from 'react';
import jwt_decode from 'jwt-decode';
import { useNavigate } from 'react-router-dom';
import { Button, Grid, TextField, Typography } from '@mui/material';

import dotenv from 'dotenv';
dotenv.config({ path: './.env' });

export const Dashboard = () => {
	const navigate = useNavigate();
	const [ quote, setQuote ] = useState('');
	const [ tempQuote, setTempQuote ] = useState('');

	const populateQuote = async () => {
		const res = await fetch(`${process.env.REACT_APP_MERN_URL}api/quote`, {
			headers: {
				'x-access-token': JSON.parse(localStorage.getItem('token')!)
			}
		});

		const data = await res.json();

		if (data.status === 'ok') {
			setQuote(data.quote);
		} else {
			alert(data.error);
		}
	};
	useEffect(() => {
		const token = localStorage.getItem('token');
		if (token) {
			const user = jwt_decode(token);
			if (!user) {
				localStorage.removeItem('token');
				navigate('/login');
			} else {
				populateQuote();
			}
		}
	}, []);

	const updateQuote = async (e: React.MouseEvent<HTMLButtonElement>) => {
		e.preventDefault();

		const res = await fetch(`${process.env.REACT_APP_MERN_URL}api/quote`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'x-access-token': JSON.parse(localStorage.getItem('token')!)
			},
			body: JSON.stringify({
				quote: tempQuote
			})
		});
		const data = await res.json();
		if (data.status === 'ok') {
			setQuote(tempQuote);
			setTempQuote('');
		} else {
			alert(data.error);
		}
	};

	return (
		<Grid
			container
			item
			md={12}
			justifyContent="center"
			alignItems="center"
			direction="column"
			sx={{ minHeight: '690px' }}
		>
			<Grid item>
				<Typography variant="h5" color="red">
					Your quote: {quote || 'NO quote found'}
				</Typography>
			</Grid>

			<br />

			<Grid container item justifyContent="center" alignItems="center" direction="column">
				<TextField
					variant="outlined"
					value={tempQuote}
					onChange={(e) => setTempQuote(e.target.value)}
					margin="normal"
					label="Enter Quote"
				/>
				<Button variant="contained" onClick={updateQuote}>
					Submit
				</Button>
				<br />
				<Button variant="contained" onClick={() => (window.location.href = '/')}>
					Log Out
				</Button>
			</Grid>
		</Grid>
	);
};
